<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Export
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/../com/adodb5/adodb.inc.php');
class Export {
    //put your code here
    protected $conn, $exportDir;
    
    public function __construct(ADOConnection $conn, $exportDir = '') {
        $this->exportDir = $exportDir;
        $this->conn = $conn;//&ADONewConnection('mysql');
        //$this->conn->PConnect('localhost', 'root', '123', 'testdrive');
        print('parse '.__CLASS__.'\n');
        $this->exportDict('category');
        $this->exportDict('region');
    }
    
    protected function exportDict($dict)
    {
        $d = fopen($this->exportDir.$dict.'.csv', 'w') or die("can't open file");
        fputcsv($d, array($dict.'_id', 'name'));
        $sql = "Select * from `i_tender_".$dict."` where `parent` = 0 order by id";
        $r = $this->conn->Execute($sql);
        while(!$r->EOF)
        {
            $fields = array($r->fields['id'], iconv('cp1251', 'utf-8', $r->fields['name']));
            fputcsv($d, $fields);
            $r->moveNext();
        }
        fclose($d);
    }
    
    public function regionDayCount()
    {
        $dates = array();
        $regions = array();
        $categorys = array();
        
        $csv = fopen($this->exportDir.'region_day_count.csv', 'w') or die("can't open file");
        
        
        fputcsv($csv, array('region_id', 'day', 'count'));
        
        $result = array();
        
        $sql = "SELECT distinct(date_end) FROM `i_tender_all` ";
        $r = $this->conn->Execute($sql);
        while(!$r->EOF)
        {
            $dates[] = $r->fields['date_end'];
            $r->moveNext();
        }
        //print_r($dates);
        $sql = "SELECT distinct(parent_region_id) as id FROM `i_tender_all` where parent_region_id > 0 order by id desc";
        //$sql = "SELECT id, ( select group_concat(id, parent) from i_tender_region where parent = self.id  ) as r from i_tender_region as self where parent = 0 order by id asc";
        $r = $this->conn->Execute($sql);
        while(!$r->EOF)
        {
            $region_id = $r->fields['id'];
            //$regions = $r->fields['r'];
            print_r("region ".$region_id."\n");
            foreach($dates as $day)
            {
                //print_r("region ".$region_id."\n");
                //$sql = "select count(*) as count from `i_tender_all` where date_end = '".$day."' and region_id  in(".$regions.") ";
                $sql = "select count(*) as count from `i_tender_all` where date_end = '".$day."' and parent_region_id = ".$region_id." ";
                //print_r($sql."\n\n\n\n");
                $c = $this->conn->Execute($sql);
                $count = $c->fields['count'];
                //print_r("\tselect count(*) as count from `i_tender_all` where date_end = '".$day."' and region_id = '".$region_id."' ");
                if($count == 0)
                    continue;
                $fields = array($region_id, $day, $count);
                fputcsv($csv, $fields);
                print_r("\t day ".$day." count ".$count." \n\n");
                $result[] = array(
                  'region_id' =>  $region_id,
                  'day' => $day,
                  'count' => $count  
                );
            }
            $r->moveNext();
        }
        fclose($csv);
        //print_r($result);
    }
    
    public function categoryDayCount()
    {
        $dates = array();
        $regions = array();
        $categorys = array();
        
        $csv = fopen($this->exportDir.'category_day_count.csv', 'w') or die("can't open file");
        fputcsv($csv, array('category_id', 'day', 'count'));
        
        $result = array();
        
        $sql = "SELECT distinct(date_end) FROM `i_tender_all` ";
        $r = $this->conn->Execute($sql);
        while(!$r->EOF)
        {
            $dates[] = $r->fields['date_end'];
            $r->moveNext();
        }
        //print_r($dates);
        $sql = "SELECT distinct(parent_category_id) as id FROM `i_tender_all` where parent_category_id > 0 order by id desc";
        //$sql = "SELECT id, ( select group_concat(id, parent) from i_tender_region where parent = self.id  ) as r from i_tender_region as self where parent = 0 order by id asc";
        $r = $this->conn->Execute($sql);
        while(!$r->EOF)
        {
            $category_id = $r->fields['id'];
            //$regions = $r->fields['r'];
            print_r("category ".$category_id."\n");
            foreach($dates as $day)
            {
                $sql = "select count(*) as count from `i_tender_all` where date_end = '".$day."' and parent_category_id = ".$category_id." ";
                $c = $this->conn->Execute($sql);
                $count = $c->fields['count'];
                if($count == 0)
                    continue;
                $fields = array($category_id, $day, $count);
                fputcsv($csv, $fields);
                print_r("\t day ".$day." count ".$count." \n\n");
                $result[] = array(
                  'category_id' =>  $category_id,
                  'day' => $day,
                  'count' => $count  
                );
            }
            $r->moveNext();
        }
        fclose($csv);
        //print_r($result);
    }
}

?>
