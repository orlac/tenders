<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Parser
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/../com/phpQuery/phpQuery.php');
require_once (dirname(__FILE__).'/../com/adodb5/adodb.inc.php');
class Parser {
    //put your code here
        protected $conn;
    
    public function __construct(ADOConnection $conn) {
        
        $this->conn = $conn;//&ADONewConnection('mysql');
        //$this->conn->PConnect('localhost', 'root', '123', 'testdrive');
        print('parse '.__CLASS__.'\n');
    }
    
    protected $rootUrl = 'http://www.i-tenders.ru/';
    protected $urlCats = 'http://www.i-tenders.ru/index.php?sectenders';
    
    protected $catPage;
    protected $cats = array();
    
    protected $table = '';
    
    public $headers = array(
            'Accept-Charset: utf-8;q=0.7,*;q=0.7',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
            );
    
    public function load($url = null, $onLoad = 'onLoad')
    {
        $url = ($url == null)? $this->urlCats : $url;
        //print_r('load page:'. $url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $catPage = curl_exec($ch); 
        $this->$onLoad($catPage);
    }
    
    private $tmpParentCat = 0;
    
    protected function onLoad($page)
    {
        $document = \phpQuery::newDocument($page);
        $elements = $document->find('table.show-list > tr');
        while($elementTr = array_shift($elements->elements))
        {
            $tds = pq($elementTr)->find('td');
            $a = pq($tds->elements[0])->find('a');
            $arr = mb_split('=', $a->attr('href'));
            
            $this->cats[$arr[1]] = array();
            $this->cats[$arr[1]]['id'] = $arr[1];
            $this->cats[$arr[1]]['url'] = $a->attr('href');
            $this->cats[$arr[1]]['name'] = $a->html();
            $this->cats[$arr[1]]['children'] = array();
            $this->tmpParentCat = $arr[1];
            //print_r("LOad cat ".$a->html()."\n");
            $this->load($this->getCatUrl($a->attr('href')), 'onLoadSubCats' );
            unset($elementTr);
        }
        //print_r($this->cats);
        $this->save($this->cats);
    }
    
    protected function onLoadSubCats($page)
    {
        $document = \phpQuery::newDocument($page);
        $trs = $document->find('table.catalog > tr > td > table > tr');
        while( $tr = array_shift($trs->elements) )
        {
            $tds = pq($tr)->find('td');
            $a = pq($tds->elements[3])->find('a');
            $arr = mb_split('=', $a->attr('href'));
            $id = $arr[1];
            $subcats = &$this->cats[$this->tmpParentCat]['children'];
            $subcats[$id] = array();
            $subcats[$id]['name'] = $a->html();
            $subcats[$id]['url'] = $a->attr('href');
            $subcats[$id]['id'] = $id;
            $subcats[$id]['children'] = array();
            //print_r("\tLOad subcat ".$a->html()."\n");
            $this->load($this->getCatUrl($subcats[$id]['url']), 'onLoadSubCats' );
        }
    }
    
    protected function getCatUrl($url)
    {
        return $this->rootUrl.$url;
    }
    
    private function save(&$cats, $parent=0)
    {
        $sql = 'REPLACE INTO '.$this->table.' (id, parent, name) ';
        $values = array();
        while($cat = array_shift($cats))
        {
            $name = str_replace("'", '', $cat['name']);
            $values[] =  '("'.$cat['id'].'", "'.$parent.'", "'.ADOConnection::addq($name) .'")';
            //print($cat['id']."      ".$parent."     ".ADOConnection::addq($name)."\n");
            if(count($cat['children']) > 0)
                $this->save ($cat['children'], $cat['id']);
        }
        if(count($values) == 0)
            return;
        $sql .= ' VALUES '. implode(',', $values);
        $this->conn->Execute($sql);
    }
}

?>
