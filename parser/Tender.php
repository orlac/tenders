<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tender
 *
 * @author Antonio
 * orlac@rambler.ru
 */
require_once (dirname(__FILE__).'/../com/phpQuery/phpQuery.php');
require_once (dirname(__FILE__).'/../com/adodb5/adodb.inc.php');
class Tender {
    //put your code here
    
    protected $page = 1;
    protected $url = 'http://www.i-tenders.ru/index.php?tenderlist=all';
    
    public $headers = array(
            'Accept-Charset: utf-8;q=0.7,*;q=0.7',
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
            );
    protected $tenders = array();
    protected $conn;
    
    public function __construct(ADOConnection $conn) {
        $this->conn = $conn;//&ADONewConnection('mysql');
        //$this->conn->PConnect('localhost', 'root', '123', 'testdrive');
        print('parse '.__CLASS__.'\n');
    }
    
    public function load()
    {
        $find = true;
        $page = $this->loadPage();
        while($find)
        {
            $count = $this->onLoad($page);
            unset($page);
            if($count == 0)
            {
                print_r('End\n');
                break;
            }
            $this->nextPage();
            $page = $this->loadPage($this->getUrlByPage($this->page));
        }
    }
    
    protected function loadPage($url = null)
    {
        $url = ($url == null)? $this->url : $url;
        print('Load page :'.$url."<br>");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HEADER, $this->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        return $page = curl_exec($ch); 
        //$this->$onLoad($page);
    }
    
    protected function onLoad($page)
    {
        $document = \phpQuery::newDocument($page);
        $trs = $document->find('table.data > tr.color0');
        $trs->elements = array_merge($trs->elements, $document->find('table.data > tr.color1')->elements );
        $tenders = array();
        while( $tr = array_shift($trs->elements) )
        {
            $tds = pq($tr)->find('td');
            $a = pq($tds->elements[0])->find('a');
            
            $arr = mb_split('=', $a->attr('href'));
            $id = $arr[1];
            
            $tenders[$id] = array();
            $tenders[$id]['id'] = $id;
            $tenders[$id]['name'] = $a->html();
            $tenders[$id]['category'] = pq($tds->elements[1])->html();
            $tenders[$id]['region'] = pq($tds->elements[2])->html();
            $tenders[$id]['summ'] = pq($tds->elements[3])->find('b')->html();
            $tenders[$id]['date_start'] = pq($tds->elements[4])->html();
            $tenders[$id]['date_end'] = pq($tds->elements[5])->html();
        }
        //print_r($this->tenders);
        $count = count($tenders);
        $this->save($tenders);
        
        unset($document);
        unset($tds);
        unset($arr);
        unset($trs);
        return $count;
    }
    
    protected function save(&$tenders)
    {
        $sql = 'REPLACE INTO `i_tender_all` (id, name, category_id, parent_category_id, region_id, parent_region_id, date_start, date_end, summ) ';
        $values = array();
        while($tender = array_shift($tenders))
        {
            $id = $tender['id'];
            $name = str_replace("'", '', $tender['name']);
            $regionId = $this->getRegionId($tender['region']);
            $catId = $this->getCategoryId($tender['category']);
            $summ = $tender['summ'];
            $date_start = date( 'Y-m-d', strtotime($tender['date_start']) );
            $date_end = date( 'Y-m-d', strtotime($tender['date_end']) );
            
            $values[] = " ('".$id."', '".ADOConnection::addq($name) ."', '".$catId['id']."', '".$catId['parent']."', '".$regionId['id']."', '".$regionId['parent']."', '".$date_start."', '".$date_end."', '".$summ."')";
        }
        if(count($values) == 0)
            return;
        $sql .= ' VALUES '. implode(',', $values);
        /*
        print_r('<hr>');
        print_r($sql);
        print_r('<hr>');
        */
        $this->conn->Execute($sql);
        print_r('addad '.count($values));
    }
    
    protected function getCategoryId($nameCategory)
    {
        $sql = "SELECT `id`, `parent` FROM `i_tender_category` WHERE `name` = '".$nameCategory."' LIMIT 1 ";
        $result = $this->conn->Execute($sql);
        return $result->fields;
    }
    
    protected function getRegionId($nameRegion)
    {
        $sql = "SELECT `id`, `parent` FROM `i_tender_region` WHERE `name` = '".$nameRegion."' LIMIT 1 ";
        $result = $this->conn->Execute($sql);
        return $result->fields;
    }
    
    protected function nextPage()
    {
        $this->page++;
    }
    
    protected function getUrlByPage($page)
    {
        return $this->url.'&page='.$page;
    }
    
}

?>
